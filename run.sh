#!/bin/bash


## Creating limesurvey configuration files
config=/app/limesurvey/application/config/config.php

echo -ne "Configuring limesurvey database config file..."
if [ -f $config ]
then
    # Old way, copying sample file and changing it
    #cp /app/limesurvey/application/config/config-sample-pgsql.php $config
    ## Replace parameters by stuff in environment variables
    #sed -i -e "s/'username' => 'postgres',/'username' => '$PGUSER',/g" $config
    #sed -i -e "s/'password' => 'somepassword',/'password' => '$PGPASS',/g" $config
    #sed -i -e "s/'connectionString' => 'pgsql:host=localhost;port=5432;user=postgres;password=somepassword;dbname=limesurvey;',/'connectionString' => 'pgsql:host=$POSTGRES_SERVICE_HOST;port=$POSTGRES_SERVICE_PORT;user=$PGUSER;password=$PGPASS;dbname=$POSTGRES_DATABASE_NAME;',/g" $config
    ## Need htis as well otherwise it does not work
    #sed -i -e "s/'urlFormat' => 'get',/'urlFormat' => 'path',/g" $config
    
    # New way with injected config file
    sed -i -e "s/POSTGRES_USER/$PGUSER/g" $config
    sed -i -e "s/POSTGRES_PASSWORD/$PGPASS/g" $config
    sed -i -e "s/POSTGRES_SERVICE_HOST/$POSTGRES_SERVICE_HOST/g" $config
    sed -i -e "s/POSTGRES_SERVICE_PORT/$POSTGRES_SERVICE_PORT/g" $config
    sed -i -e "s/POSTGRES_DATABASE_NAME/$POSTGRES_DATABASE_NAME/g" $config
    
    echo OK
else
    echo SKIPPING as config file is missing
fi

#itail -F /var/log/apache2/* &
exec httpd -DFOREGROUND