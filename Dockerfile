FROM centos:centos7

MAINTAINER "Emmanuel Ormancey <emmanuel.ormancey@cern.ch>"

# -----------------------------------------------------------------------------
# Apache + PHP
# -----------------------------------------------------------------------------
RUN	yum -y update \
	&& yum --setopt=tsflags=nodocs -y install \
	httpd \
	unzip

RUN yum --setopt=tsflags=nodocs -y install php php-mysql php-pgsql php-gd php-ldap php-xml php-xmlrpc php-mbstring php-mcrypt curl zlib libtool-ltdl php-pdo

# IMAP is in epel-release ...
RUN yum --setopt=tsflags=nodocs -y install epel-release 
RUN yum --setopt=tsflags=nodocs -y install php-imap

RUN rm -rf /var/cache/yum/* && yum clean all

RUN rm -rf /app && mkdir -p /app 
#RUN curl -L -o /app/limesurvey.tar.gz "https://www.limesurvey.org/stable-release?download=1858:limesurvey252%20160920targz"
#RUN curl -L -o /app/limesurvey.tar.gz "https://www.limesurvey.org/stable-release?download=1873:limesurvey2541%20161010targz"
RUN curl -L -o /app/limesurvey.tar.gz "https://www.limesurvey.org/stable-release?download=1969:limesurvey2582%20170114targz"

RUN tar -C /app -xf /app/limesurvey.tar.gz
RUN rm /app/limesurvey.tar.gz
#RUN chown -R www-data:www-data /app 
#RUN chown www-data:www-data /var/lib/php5 
#RUN cp -r /app/limesurvey/application/config /app/limesurvey/application/config-sample
COPY config.php /app/limesurvey/application/config/

#COPY apache_default /etc/httpd/sites-available/000-default.conf
#COPY apache_default /etc/httpd/sites-enabled/000-default.conf 
COPY apache_default /etc/httpd/conf.d/limesurvey.conf 
#RUN sed -i "s/AllowOverride None/AllowOverride All/g" /etc/httpd/httpd.conf
RUN sed -i "s/Listen 80/Listen 8080/g" /etc/httpd/conf/httpd.conf
#RUN sed -i "s/Listen 80/Listen 8080/g" /etc/httpd/ports.conf

# permissions for obscure user running apache
RUN chmod -R a+rwx /app/limesurvey/tmp
RUN chmod -R a+rwx /app/limesurvey/upload
RUN chmod -R a+rwx /app/limesurvey/application/config

RUN chmod -R a+rwx /var/lib/php/session

# Gore
RUN mkdir -p /var/log/httpd && chown -R root:root /var/log/httpd && chmod -R a+rwx /var/log/httpd
RUN mkdir -p /var/run/httpd && chown -R root:root /var/run/httpd && chmod -R a+rwx /var/run/httpd
RUN mkdir -p /var/lock/httpd && chown -R root:root /var/lock/httpd && chmod -R a+rwx /var/lock/httpd

COPY run.sh /run.sh
RUN chmod a+x /run.sh
CMD ["/run.sh"] 

EXPOSE 8080
